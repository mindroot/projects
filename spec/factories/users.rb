FactoryBot.define do
  factory :user do
    first_name { FFaker::Name.first_name }
    last_name  { FFaker::Name.last_name }
    username  { first_name }
    email { FFaker::Internet.email }
    password { "test42" }
    admin { false }
    confirmed_at { Time.now }
  end
end
